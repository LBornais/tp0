from character import Character

import random


class Hero(Character):
    """Default hero character
    """

    def __init__(self, name, life, power):
        super().__init__("Hero", name, life, power)

    def fight(self):
        # by default hero always has 
        return random.randint(5, 10) * self.power

    def update_stats(self):
        # by default the hero will always hit harder
        self.power *= 1.2

class Mage(Hero):

    def __init__(self):
        super().__init__("Mage", 200, 1)

    def fight(self):
        # by default hero always has 
        return random.randint(20, 40) * self.power

class Knight(Hero):
    def __init__(self):
        super().__init__("Knight", 100, 2)
        self.n_turn = 0

    def get_damage(self, damage):
        if(self.n_turn < 3):
            damage = damage/2

        print(f'[{self.kind}] - {self.name} took {damage:.3f} damage')

        self.life -= damage

        if self.life < 0:
            self.life = 0

        self.n_turn = self.n_turn + 1

class Viking(Hero):
    def __init__(self):
        super().__init__("Viking", 150, 6)

    def fight(self):
        if random.random() < 0.03:
            return self.power * 20
        else:
            return 3 * self.power

