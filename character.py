from abc import abstractmethod

class Character():

    def __init__(self, kind, name, life, power):
        self.kind = kind
        self.name = name
        self.life = life
        self.power = power

    @abstractmethod
    def fight(self):
        """Enter in fight mode and give hit to opponent

        Return: {int} -- damage infliged
        """
        pass

    @abstractmethod
    def update_stats(self):
        """Specific boost behavior of character
        """
        pass

    def get_damage(self, damage):

        print(f'[{self.kind}] - {self.name} took {damage:.3f} damage')
        
        self.life -= damage

        if self.life < 0:
            self.life = 0

    def is_alive(self):
        """Check if character still alive

        Returns:
            [bool]: life state of character
        """
        return self.life > 0

    def __str__(self):
        return f'[{self.kind}] - {self.name} has {self.life:.3f} life and power of {self.power:.3f}'