from hero import *
from monster import *


import time

def main():

    # create new hero
    #hero = Hero('Zeus', 200, 2)
    #monster = Monster('Kraken', 400, 1)
    hero = Viking()
    monster = Dark_Lord_Sauron()
    print("Let's the fight begin !!")

    n_turn = 0
    while hero.is_alive() and monster.is_alive():

        print('-----------')
        print(f'Begin of turn {n_turn + 1}:')

        # hero starts
        monster.get_damage(hero.fight())

        # monster gives answer
        hero.get_damage(monster.fight())

        time.sleep(1)

        print('-----------')
        print(f'End of turn {n_turn + 1}:')
        print(f' - {hero}')
        print(f' - {monster}')
        print('-----------')

        hero.update_stats()
        monster.update_stats()

        n_turn += 1

    winner = None
    if hero.is_alive():
        winner = hero
    else:
        winner = monster

    print(f'Winner is {winner.name}')

if __name__ == "__main__":
    main()