from character import Character

import random


class Monster(Character):
    """Default monster character
    """

    def __init__(self, name, life, power):
        super().__init__("Monster", name, life, power)

    def fight(self):
        # by default monster can inflict huge hit 
        return random.randint(0, 20) * self.power

    def update_stats(self):
        # by default the monster not always gain in power
        if random.random() > 0.5:
            self.power *= 1.05


class Dragon(Monster):

    def __init__(self):
        super().__init__("Dragon", 400, 1)

    def get_damage(self, damage):

        if random.random() > 0.3:
            print(f'[{self.kind}] - {self.name} took {damage:.3f} damage')
            self.life -= damage
        else:
            print(f'[{self.kind}] - {self.name} avoided the hit !')

        if self.life < 0:
            self.life = 0

class Orc(Monster):

    def __init__(self):
        super().__init__("Orc", 120, 3)

    def fight(self):
        if random.random() < 0.05:
            return 10 * self.power
        else : 
            return self.power

class Dark_Lord_Sauron(Monster):
    """Dark Lord Sauron Monster
    """
    def __init__(self):
        super().__init__("Dark Lord Sauron", 900, 10)

    def fight(self):
        if random.random() < 0.03:
            return 20 * self.power
        else :
            return self.power * 3

    def get_damage(self, damage):
        if random.random() > 0.4:
            print(f'[{self.kind}] - {self.name} took {damage:.3f} damage')
            self.life -= damage
        else:
            print(f'[{self.kind}] - {self.name} avoided the attack !')

        if self.life < 0:
            self.life = 0

    
